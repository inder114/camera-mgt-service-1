package stream.camera.management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import stream.camera.management.model.Salt;

@Repository
public interface SaltRepository extends JpaRepository<Salt, String> {
}
