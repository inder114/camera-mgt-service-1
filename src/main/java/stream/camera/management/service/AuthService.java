package stream.camera.management.service;

import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stream.camera.management.dto.AuthDto;
import stream.camera.management.dto.AuthPlainDto;
import stream.camera.management.model.Auth;
import stream.camera.management.model.Salt;
import stream.camera.management.repository.AuthRepository;
import stream.camera.management.repository.SaltRepository;
import stream.camera.management.util.AESUtil;
import stream.camera.management.util.UtilMapper;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

@Service
public class AuthService {

    private AuthRepository authRepository;
    private SaltRepository saltRepository;
    private SecretKey secretKey;
    private final Logger log = LoggerFactory.getLogger(AuthService.class);

    @Autowired
    private UtilMapper utilMapper;

    @Autowired
    public AuthService(AuthRepository authRepository, SaltRepository saltRepository) throws NoSuchAlgorithmException {
        this.authRepository = authRepository;
        this.saltRepository = saltRepository;
        String secretKeyString = saltRepository.findAll().size() == 0 ? "" : saltRepository.findAll().get(0).getSecretKey();
        if (secretKeyString == null || secretKeyString.isEmpty()) {
            secretKey = AESUtil.generateKey();
            String encodedKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());
            saltRepository.save(new Salt(encodedKey));
        } else {
            byte[] decodeKey = Base64.getDecoder().decode(secretKeyString);
            secretKey = new SecretKeySpec(decodeKey, 0, decodeKey.length, AESUtil.ALGORITHM);
        }
        System.out.println(secretKey);
    }

    public List<AuthPlainDto> getAllAuth() throws Exception {
        List<Auth> authList = authRepository.findAll();
        log.info("Successfully get auth rows from database");
        List<AuthPlainDto> authPlainDtoList = new ArrayList<>();
        for (Auth auth : authList) {
            AuthDto authDto = new AuthDto();
            utilMapper.mapAuthToAuthDto(auth, authDto);
            String decodedUsername = "";
            String decodedPassword = "";

            if (authDto.getUsername() != null && !authDto.getUsername().isEmpty())
                decodedUsername = AESUtil.decrypt(authDto.getUsername(), secretKey);
            if (authDto.getPassword() != null && !authDto.getPassword().isEmpty())
                decodedPassword = AESUtil.decrypt(authDto.getPassword(), secretKey);

            log.debug("decodedUsername is {}, decodedPassword is {}", decodedUsername, decodedPassword);
            AuthPlainDto authPlainDto = new AuthPlainDto(authDto.getUuid(), authDto.getIpAddressList(), decodedUsername, decodedPassword);
            authPlainDtoList.add(authPlainDto);
        }
        return authPlainDtoList;
    }

    public AuthPlainDto getAuthByUuid(String uuid) throws Exception {
        Auth auth = authRepository.findByUuid(uuid);
        if (auth != null) {
            String decodedUsername = null;
            String decodedPassword = null;
            if (auth.getUsername() != null && !auth.getUsername().isEmpty())
                decodedUsername = AESUtil.decrypt(auth.getUsername(), secretKey);
            if (auth.getPassword() != null && !auth.getPassword().isEmpty())
                decodedPassword = AESUtil.decrypt(auth.getPassword(), secretKey);

            log.debug("Successfully get auth row by uuid from database");
            AuthDto authDto = new AuthDto();
            utilMapper.mapAuthToAuthDto(auth, authDto);
            return new AuthPlainDto(authDto.getUuid(), authDto.getIpAddressList(), decodedUsername, decodedPassword);
        } else
            return null;
    }

    public void createAuth(List<AuthDto> authDtoList) throws Exception {
        for (AuthDto authDto : authDtoList) {
            String encodeUsername = "";
            String encodePassword = "";

            if (authDto.getUsername() != null && !authDto.getUsername().isEmpty())
                encodeUsername = AESUtil.encrypt(authDto.getUsername(), secretKey);
            if (authDto.getPassword() != null && !authDto.getPassword().isEmpty())
                encodePassword = AESUtil.encrypt(authDto.getPassword(), secretKey);

            log.info("encodeUsername is {}, encodePassword is {}", encodeUsername, encodePassword);

            Auth auth = new Auth(authDto.getUuid(), authDto.getIpAddressList(), encodeUsername, encodePassword,
                    LocalDate.now(), authDto.getCreateBy(), null, null);
            this.authRepository.save(auth);
            log.debug("Successfully save auth row into database");
        }
    }

    public void updateAuth(List<AuthDto> authDtoList) throws Exception {
        for (AuthDto authDto : authDtoList) {
            Auth originAuth = authRepository.findByUuid(authDto.getUuid());
            log.debug("Successfully get auth row from database");

            String[] ipAddress = authDto.getIpAddressList() == null ? originAuth.getIpAddressList() : authDto.getIpAddressList();
            String username = null;
            String password = null;
            if (authDto.getUsername() == null) {
                username = originAuth.getUsername();
            } else {
                username = AESUtil.encrypt(authDto.getUsername(), secretKey);
            }

            if (authDto.getPassword() == null) {
                password = originAuth.getPassword();
            } else {
                password = AESUtil.encrypt(authDto.getPassword(), secretKey);
            }

            LocalDate createAt = authDto.getCreateAt() == null ? originAuth.getCreateAt() : authDto.getCreateAt();
            String createBy = authDto.getCreateBy() == null ? originAuth.getCreateBy() : authDto.getCreateBy();
            String updateBy = authDto.getUpdateBy() == null ? originAuth.getUpdateBy() : authDto.getUpdateBy();

            Auth auth = new Auth(authDto.getUuid(), ipAddress, username, password, createAt, createBy, LocalDate.now(), updateBy);
            this.authRepository.save(auth);
            log.debug("Successfully save auth row into database");
        }
    }

    public void deleteAuth(List<String> uuidList) {
        for (String uuid : uuidList) {
            Auth auth = authRepository.findByUuid(uuid);
            log.debug("Successfully get auth row by uuid from database");
            AuthDto authDto = new AuthDto();
            utilMapper.mapAuthToAuthDto(auth, authDto);
            authRepository.delete(auth);
            log.debug("Successfully delete auth row from database");
        }
    }

    public void deleteAllAuth() {
        authRepository.deleteAll();
        log.debug("Successfully delete all auth rows in database");
    }
}
