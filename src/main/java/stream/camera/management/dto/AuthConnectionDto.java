package stream.camera.management.dto;

public class AuthConnectionDto {
    private String uuid;
    private String ipAddress;
    private String username;
    private String password;

    public AuthConnectionDto() {
        super();
    }

    public AuthConnectionDto(String uuid, String ipAddress, String username, String password) {
        this.uuid = uuid;
        this.ipAddress = ipAddress;
        this.username = username;
        this.password = password;
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "{" + " \"uuid\":\"" + uuid + "\"" + ", \"ipAddress\":\"" + ipAddress + "\""
                + ", \"username\":\"" + username + "\"" + ", \"password\":\"" + password + "\"" + "}";
    }
}
